function image_upload_handler (blobInfo, success, failure) {
      success("data:" + blobInfo.blob().type + ";base64," + blobInfo.base64());
    };

function no_blob (img) {
      return !img.hasAttribute('internal-blob');
    };

tinymce.init({
	/* replace textarea having class .tinymce with tinymce editor */
	selector: ".editor",

    cleanup: true,
    convert_newlines_to_brs : false,
    remove_linebreaks: true,
    force_br_newlines : true,
    force_p_newlines : false,
    forced_root_block : false,
    inline_styles : false,
    entity_encoding: 'raw',
    entities: '160,nbsp,38,amp,60,lt,62,gt',
    paste_auto_cleanup_on_paste : true,
    paste_remove_styles: true,
    paste_remove_styles_if_webkit: true,
    paste_strip_class_attributes: true,
    relative_urls : false,
    remove_script_host : false ,
    convert_urls:true,

	/* width and height of the editor */
	width: '100%',
	height: 500,

	/* display statusbar */
	statusbar: true,

	/* plugin */
	plugins: [
		"advlist autolink link image lists charmap print preview hr anchor pagebreak",
		"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
		"save table contextmenu directionality emoticons template paste textcolor imagetools bdeskphoto"
	],

	/* toolbar */
	toolbar: "insertfile undo redo | styleselect | fontselect fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image bdeskphoto | print preview media fullpage | forecolor backcolor emoticons",

    images_dataimg_filter: no_blob,
    paste_data_images: true,
    images_upload_handler: image_upload_handler,
    apply_source_formatting : true,
    extended_valid_elements : 'img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name]',

    formats: {
    removeformat: [
      {selector: 'b,strong,em,i,font,u,strike', remove : 'all', split : true, expand : false, block_expand: true, deep : true},
      {selector: 'span', attributes : ['style', 'class'], remove : 'empty', split : true, expand : false, deep : true},
      {selector: '*', attributes : ['style', 'class'], split : false, expand : false, deep : true}
    ]
  },

  font_formats:
    "Andale Mono=andale mono,times; Calibri=calibri,arial,helvetica,sans-serif; Arial=arial,helvetica,sans-serif; Arial Black=arial black,avant garde; Book Antiqua=book antiqua,palatino; Comic Sans MS=comic sans ms,sans-serif; Courier New=courier new,courier; Georgia=georgia,palatino; Helvetica=helvetica; Impact=impact,chicago; Symbol=symbol; Tahoma=tahoma,arial,helvetica,sans-serif; Terminal=terminal,monaco; Times New Roman=times new roman,times; Trebuchet MS=trebuchet ms,geneva; Verdana=verdana,geneva; Webdings=webdings; Wingdings=wingdings,zapf dingbats",

    fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt',

	/* style */
	style_formats: [
		{title: "Headers", items: [
			{title: "Header 1", format: "h1"},
			{title: "Header 2", format: "h2"},
			{title: "Header 3", format: "h3"},
			{title: "Header 4", format: "h4"},
			{title: "Header 5", format: "h5"},
			{title: "Header 6", format: "h6"}
		]},
		{title: "Inline", items: [
			{title: "Bold", icon: "bold", format: "bold"},
			{title: "Italic", icon: "italic", format: "italic"},
			{title: "Underline", icon: "underline", format: "underline"},
			{title: "Strikethrough", icon: "strikethrough", format: "strikethrough"},
			{title: "Superscript", icon: "superscript", format: "superscript"},
			{title: "Subscript", icon: "subscript", format: "subscript"},
			{title: "Code", icon: "code", format: "code"}
		]},
		{title: "Alignment", items: [
			{title: "Left", icon: "alignleft", format: "alignleft"},
			{title: "Center", icon: "aligncenter", format: "aligncenter"},
			{title: "Right", icon: "alignright", format: "alignright"},
			{title: "Justify", icon: "alignjustify", format: "alignjustify"}
		]}
	]

});