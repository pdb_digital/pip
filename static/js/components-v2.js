window.Components = {
  customSelect(options) {
    return {
      init() {
        this.optionCount = this.$refs.listbox.children.length
        this.$watch('selected', (value) => {
          if (!this.open) return

          if (this.selected === null) {
            this.activeDescendant = ''
            return
          }

          this.activeDescendant = this.$refs.listbox.children[this.selected].id
        })
      },
      activeDescendant: null,
      optionCount: null,
      open: false,
      selected: null,
      value: 0,
      choose(option) {
        this.value = option
        this.open = false
      },
      onButtonClick() {
        if (this.open) return
        this.selected = this.value
        this.open = true
        this.$nextTick(() => {
          this.$refs.listbox.focus()
          this.$refs.listbox.children[this.selected].scrollIntoView({ block: 'nearest' })
        })
      },
      onOptionSelect() {
        if (this.selected !== null) {
          this.value = this.selected
        }
        this.open = false
        this.$refs.button.focus()
      },
      onEscape() {
        this.open = false
        this.$refs.button.focus()
      },
      onArrowUp() {
        this.selected = this.selected - 1 < 0 ? this.optionCount - 1 : this.selected - 1
        this.$refs.listbox.children[this.selected].scrollIntoView({ block: 'nearest' })
      },
      onArrowDown() {
        this.selected = this.selected + 1 > this.optionCount - 1 ? 1 : this.selected + 1
        this.$refs.listbox.children[this.selected].scrollIntoView({ block: 'nearest' })
      },
      ...options,
    }
  },
  customSelect2(options) {
    let modelName = options.modelName || 'selected'

    return {
      init() {
        this.optionCount = this.$refs.listbox.children.length
        this.$watch('activeIndex', (value) => {
          if (!this.open) return

          if (this.activeIndex === null) {
            this.activeDescendant = ''
            return
          }

          this.activeDescendant = this.$refs.listbox.children[this.activeIndex].id
        })
      },
      activeDescendant: null,
      optionCount: null,
      open: false,
      activeIndex: null,
      selectedIndex: 0,
      get active() {
        return this.items[this.activeIndex]
      },
      get [modelName]() {
        return this.items[this.selectedIndex]
      },
      choose(option) {
        this.selectedIndex = option
        this.open = false
      },
      onButtonClick() {
        if (this.open) return
        this.activeIndex = this.selectedIndex
        this.open = true
        this.$nextTick(() => {
          this.$refs.listbox.focus()
          this.$refs.listbox.children[this.activeIndex].scrollIntoView({ block: 'nearest' })
        })
      },
      onOptionSelect() {
        if (this.activeIndex !== null) {
          this.selectedIndex = this.activeIndex
        }
        this.open = false
        this.$refs.button.focus()
      },
      onEscape() {
        this.open = false
        this.$refs.button.focus()
      },
      onArrowUp() {
        this.activeIndex = this.activeIndex - 1 < 0 ? this.optionCount - 1 : this.activeIndex - 1
        this.$refs.listbox.children[this.activeIndex].scrollIntoView({ block: 'nearest' })
      },
      onArrowDown() {
        this.activeIndex = this.activeIndex + 1 > this.optionCount - 1 ? 0 : this.activeIndex + 1
        this.$refs.listbox.children[this.activeIndex].scrollIntoView({ block: 'nearest' })
      },
      ...options,
    }
  },
}
