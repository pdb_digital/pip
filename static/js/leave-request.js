const date = new Date();
const selectedDays = document.querySelector(".days .day");
const renderCalendar = () => {
  date.setDate(1);

  const monthDays = document.querySelector(".days");

  const lastDay = new Date(
    date.getFullYear(),
    date.getMonth() + 1,
    0
  ).getDate();

  const prevLastDay = new Date(
    date.getFullYear(),
    date.getMonth(),
    0
  ).getDate();

  const firstDayIndex = date.getDay();
  
  const lastDayIndex = new Date(
    date.getFullYear(),
    date.getMonth() + 1,
    0
  ).getDay();

  const nextDays = 7 - lastDayIndex - 1;

  const months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];

  document.querySelector(".date h1").innerHTML = months[date.getMonth()] + " " + date.getFullYear();
  document.querySelector(".date p").innerHTML = "Today's Date - " + new Date().toDateString();

  monthDays.innerHTML = "";

  // adding the previous month's dates
  for (let x = firstDayIndex; x > 0; x--) {
    const day_element = document.createElement('div');
    day_element.classList.add('prev-date');
    day_element.textContent = prevLastDay - x + 1;
    monthDays.appendChild(day_element);

    console.log(firstDayIndex);
  }


  let date_value = new Array();
  let duration = 0;


  for (let i = 1; i <= lastDay; i++) {

    console.log(i);
    
    // Adding the current month's dates
    const day_element = document.createElement('div');
    day_element.classList.add('day');
    day_element.textContent = i;
    monthDays.appendChild(day_element);

     day_element.addEventListener("click", () => {  

      if(day_element.classList.toggle("today") == true){
        
        // if date toggle is true, it adds +1 to duration
        duration += 1;
        date_value.push(" "+ day_element.textContent + "/" + (date.getMonth() + 1) + "/" + date.getFullYear());
        document.querySelector("#duration").value = duration;
        for (let k = 1; k <= date_value.length; k++) {
           document.querySelector(".date-selection").value = date_value;
        }

      }else{

        // if date toggle is false, it minus -1 to duration
        duration -= 1;
        date_value.pop();
        document.querySelector("#duration").value = duration;
        for (let k = 1; k <= date_value.length; k++) {
           document.querySelector(".date-selection").value = date_value;
        }
      }

      // If there is no date selected, Input form will show no date selected
      if(date_value.length === 0){
        
        document.querySelector(".date-selection").value = "No Date Selected";

      }

     });

     // Clear Form Section
    document.querySelector(".clear-form").addEventListener("click", () => {
      day_element.classList.remove("today");
      duration = 0;
      while(date_value.length > 0) {
         date_value.pop(); 
        }
    });

  }

  // adding the next month's dates
  for (let j = 1; j <= nextDays; j++) {
    const day_element = document.createElement('div');
    day_element.classList.add('next-date');
    day_element.textContent = j;
    monthDays.appendChild(day_element);
  }

};

document.querySelector(".prev").addEventListener("click", () => {
  date.setMonth(date.getMonth() - 1);
  renderCalendar();
});

document.querySelector(".next").addEventListener("click", () => {
  date.setMonth(date.getMonth() + 1);
  renderCalendar();
});

renderCalendar();

document.querySelector(".clear-form").addEventListener("click", () => {
  document.querySelector(".leave-form").reset();
});


document.querySelector(".program").addEventListener('click', () => {
    if (document.querySelector(".program").value == "Education") {
        document.querySelector('.files').required = true;
    }
    else {
        document.querySelector('.files').required = false;
    }

});

document.querySelector(".program").addEventListener('click', () => {
    if (document.querySelector(".program").value == "Annual") {
        document.querySelector('.files').required = true;
    }
    else {
        document.querySelector('.files').required = false;
    }

});

document.querySelector(".program").addEventListener('click', () => {
    if (document.querySelector(".program").value == "Emergency") {
        document.querySelector('.files').required = true;
    }
    else {
        document.querySelector('.files').required = false;
    }

});

document.querySelector(".program").addEventListener('click', () => {
    if (document.querySelector(".program").value == "Medical") {
        document.querySelector('.files').required = true;
    }
    else {
        document.querySelector('.files').required = false;
    }

});

var formOK = false;

function validatePDF(objFileControl){
 var file = objFileControl.value;
 var len = file.length;
 var ext = file.slice(len - 4, len);
 if(ext.toUpperCase() == ".PDF"){
   formOK = true;
   console.log("File is accepted");
 }
 else{
   formOK = false;
	   if (document.querySelector(".program").value == "Education")
	   {
		alert("Please upload University letter as supporting documents. (PDF only)");
	   }
	   
	   else if (document.querySelector(".program").value == "Medical")
	   {
		alert("Please upload MC as supporting documents. (PDF only)");
	   }
     console.log("File is not accepted");
 }
}