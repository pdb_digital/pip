module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      'xs': '475px',
      '3xl': '1600px',
    },
    colors: {
      green: {
        dark: '#00A19C' 
			 }
		},
    order: {
      first: '-9999',
      last: '9999',
     none: '0',
     normal: '0',
      '1': '1',
      '2': '2',
    },
    screens: {
      'sm': '640px',
      // => @media (min-width: 640px) { ... }

      'md': '768px',
      // => @media (min-width: 768px) { ... }

      'lg': '1024px',
      // => @media (min-width: 1024px) { ... }

      'xl': '1280px',
      // => @media (min-width: 1280px) { ... }

      '2xl': '1536px',
      // => @media (min-width: 1536px) { ... }
    
    },

  },
  variants: {
    extend: {},
  },
  plugins: [require('@tailwindcss/forms'),],
  
  
}
