-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 28, 2021 at 04:39 AM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 8.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pip_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendance_sheet`
--

CREATE TABLE `attendance_sheet` (
  `id` int(11) NOT NULL,
  `attendance_month` varchar(200) NOT NULL,
  `attendance` int(11) DEFAULT NULL,
  `html_file` text DEFAULT NULL,
  `pdf_file` blob DEFAULT NULL,
  `status` varchar(120) DEFAULT NULL,
  `intern_sign` varchar(200) DEFAULT NULL,
  `sv_sign` varchar(200) DEFAULT NULL,
  `intern_id` int(11) NOT NULL,
  `last_modified` datetime DEFAULT current_timestamp(),
  `approved_date` date DEFAULT NULL,
  `submitted_date` date DEFAULT NULL,
  `filename` varchar(120) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `document_uploads`
--

CREATE TABLE `document_uploads` (
  `id` int(11) NOT NULL,
  `type` varchar(120) DEFAULT NULL,
  `filename` varchar(120) DEFAULT NULL,
  `data` longblob DEFAULT NULL,
  `folder_path` varchar(120) DEFAULT NULL,
  `intern_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `feedback_form`
--

CREATE TABLE `feedback_form` (
  `id` int(11) NOT NULL,
  `item1` varchar(120) DEFAULT NULL,
  `item2` varchar(120) DEFAULT NULL,
  `item3` varchar(120) DEFAULT NULL,
  `item4` varchar(120) DEFAULT NULL,
  `item5` varchar(120) DEFAULT NULL,
  `item6` varchar(120) DEFAULT NULL,
  `item7` varchar(120) DEFAULT NULL,
  `item8` varchar(120) DEFAULT NULL,
  `item9` varchar(120) DEFAULT NULL,
  `item10` varchar(120) DEFAULT NULL,
  `item11` varchar(120) DEFAULT NULL,
  `item12` varchar(120) DEFAULT NULL,
  `item13` varchar(120) DEFAULT NULL,
  `item14` varchar(120) DEFAULT NULL,
  `intern_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `hr_executives`
--

CREATE TABLE `hr_executives` (
  `id` int(11) NOT NULL,
  `hr_name` varchar(200) NOT NULL,
  `password` varchar(120) NOT NULL,
  `hr_email` varchar(120) NOT NULL,
  `location` varchar(200) DEFAULT NULL,
  `department` varchar(200) DEFAULT NULL,
  `division` varchar(200) DEFAULT NULL,
  `register_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `last_login` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hr_executives`
--

INSERT INTO `hr_executives` (`id`, `hr_name`, `password`, `hr_email`, `location`, `department`, `division`, `register_at`, `last_login`) VALUES
(1, 'Nadia Nordin', '123456', 'travismoney.m@gmail.com', 'Level 32, Tower 1, PETRONAS Twin Towers, KLCC', 'Human Resource Management', 'HRM', '2021-08-14 06:43:51', '2021-10-05 15:23:41'),
(2, 'Tengku Syazwani', '1a2b3c4d5e', 'tengkusyazwani.raja@petronas.com.my', 'Level 32, Tower 1, PETRONAS Twin Towers, KLCC', 'Human Resource Management', 'HRM', '2021-10-05 06:18:05', '2021-10-15 13:09:05');

-- --------------------------------------------------------

--
-- Table structure for table `interns`
--

CREATE TABLE `interns` (
  `id` int(11) NOT NULL,
  `fullname` varchar(200) NOT NULL,
  `password` varchar(120) NOT NULL,
  `gender` varchar(120) NOT NULL,
  `nationality` varchar(120) NOT NULL,
  `email` varchar(120) NOT NULL,
  `petronas_email` varchar(120) DEFAULT NULL,
  `cost_centre` varchar(120) DEFAULT NULL,
  `contact_no` varchar(120) NOT NULL,
  `nric` varchar(120) NOT NULL,
  `university` varchar(120) NOT NULL,
  `discipline` varchar(120) NOT NULL,
  `CGPA` varchar(120) NOT NULL,
  `start_date` varchar(120) NOT NULL,
  `end_date` varchar(120) NOT NULL,
  `period` varchar(120) DEFAULT NULL,
  `scholar` varchar(120) NOT NULL,
  `bank` varchar(200) DEFAULT NULL,
  `intern_dept` varchar(120) NOT NULL,
  `intern_div` varchar(120) NOT NULL,
  `register_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `last_login` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `sv_id` int(11) DEFAULT NULL,
  `hr_id` int(11) DEFAULT NULL,
  `checklist_status` varchar(120) DEFAULT NULL,
  `hr_additional_email` varchar(120) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `interns`
--

INSERT INTO `interns` (`id`, `fullname`, `password`, `gender`, `nationality`, `email`, `petronas_email`, `cost_centre`, `contact_no`, `nric`, `university`, `discipline`, `CGPA`, `start_date`, `end_date`, `period`, `scholar`, `bank`, `intern_dept`, `intern_div`, `register_at`, `last_login`, `sv_id`, `hr_id`, `checklist_status`, `hr_additional_email`) VALUES
(6, 'Travis Iran Money', '$2b$12$G7vlg1WnY9TCtkULTPS5xOjy4X/dk1LzgtExD6wYSswCUfoZRvBVa', 'Male', 'Malaysian', 'travismoney.tm@gmail.com', 'travisiran.money@petronas.com.my', 'C00444211', '0128486654', '971120135105', 'Universiti Malaysia Sarawak (UNIMAS)', 'Software Engineering', '3.95', '2021-08-02', '2021-10-22', NULL, 'None', '162731137834', 'Digital Innovations', 'Digital', '2021-08-19 03:16:16', '2021-12-28 11:31:18', 2, 1, 'submitted', 'travismoney.tm@gmail.com'),
(13, 'Nur Nadhirah Shahrol', '$2b$12$UdTdnSsBD5PhFvv9xT7U6.gFLVYXqyFNU2r4dbvap.8RCc75sy80K', 'Female', 'Malaysian', 'nadhirah.shahrol@gmail.com', 'nurnadhirah.shahrolm@petronas.com.my', 'C00444212', '0123870153', '970522085544', 'Universiti Kebangsaan Malaysia (UKM)', 'Information Technology', '3.88', '2021-09-01', '2022-01-14', NULL, 'None', 'None', 'Digital Innovations', 'Digital', '2021-10-20 03:08:11', '2021-11-23 16:26:07', 2, 1, NULL, 'travismoney.tm@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `onboarding_checklist`
--

CREATE TABLE `onboarding_checklist` (
  `id` int(11) NOT NULL,
  `item_1` varchar(120) DEFAULT NULL,
  `item_2` varchar(120) DEFAULT NULL,
  `item_3` varchar(120) DEFAULT NULL,
  `item_4` varchar(120) DEFAULT NULL,
  `item_5` varchar(120) DEFAULT NULL,
  `item_6` varchar(120) DEFAULT NULL,
  `item_7` varchar(120) DEFAULT NULL,
  `item_8` varchar(120) DEFAULT NULL,
  `item_9` varchar(120) DEFAULT NULL,
  `item_10` varchar(120) DEFAULT NULL,
  `item_11` varchar(120) DEFAULT NULL,
  `item_12` varchar(120) DEFAULT NULL,
  `completion` int(11) DEFAULT 0,
  `count1` int(11) DEFAULT 0,
  `count2` int(11) DEFAULT 0,
  `count3` int(11) DEFAULT 0,
  `count4` int(11) DEFAULT 0,
  `count5` int(11) DEFAULT 0,
  `remarks` text DEFAULT NULL,
  `status` varchar(120) DEFAULT NULL,
  `intern_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `request_leave`
--

CREATE TABLE `request_leave` (
  `id` int(11) NOT NULL,
  `selected_date` varchar(255) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `emergency_name` varchar(255) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `relation` varchar(255) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `type_program` varchar(255) DEFAULT NULL,
  `justification` text DEFAULT NULL,
  `intern_sign` varchar(200) DEFAULT NULL,
  `sv_sign` varchar(200) DEFAULT NULL,
  `sv_approval` varchar(120) DEFAULT NULL,
  `sv_approval_date` datetime(6) DEFAULT NULL,
  `hod_sign` varchar(200) DEFAULT NULL,
  `hod_approval` varchar(120) DEFAULT NULL,
  `hod_approval_date` datetime(6) DEFAULT NULL,
  `intern_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `supervisor`
--

CREATE TABLE `supervisor` (
  `id` int(11) NOT NULL,
  `sv_name` varchar(200) NOT NULL,
  `sv_email` varchar(120) DEFAULT NULL,
  `designation` varchar(120) DEFAULT NULL,
  `location` varchar(200) DEFAULT NULL,
  `department` varchar(200) DEFAULT NULL,
  `division` varchar(200) DEFAULT NULL,
  `register_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `supervisor`
--

INSERT INTO `supervisor` (`id`, `sv_name`, `sv_email`, `designation`, `location`, `department`, `division`, `register_at`) VALUES
(2, 'Fairuz Razali', 'travismoney.tm@gmail.com', 'Head Product Specialist', 'Level 32, Tower 1, PETRONAS Twin Tower, Kuala Lumpur City Center', 'Digital Innovations', 'Digital', '2021-08-17 11:22:07');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendance_sheet`
--
ALTER TABLE `attendance_sheet`
  ADD PRIMARY KEY (`id`),
  ADD KEY `intern_id` (`intern_id`);

--
-- Indexes for table `document_uploads`
--
ALTER TABLE `document_uploads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `intern_id` (`intern_id`);

--
-- Indexes for table `feedback_form`
--
ALTER TABLE `feedback_form`
  ADD PRIMARY KEY (`id`),
  ADD KEY `intern_id` (`intern_id`);

--
-- Indexes for table `hr_executives`
--
ALTER TABLE `hr_executives`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `interns`
--
ALTER TABLE `interns`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sv_id` (`sv_id`),
  ADD KEY `hr_id` (`hr_id`);

--
-- Indexes for table `onboarding_checklist`
--
ALTER TABLE `onboarding_checklist`
  ADD PRIMARY KEY (`id`),
  ADD KEY `intern_id` (`intern_id`);

--
-- Indexes for table `request_leave`
--
ALTER TABLE `request_leave`
  ADD PRIMARY KEY (`id`),
  ADD KEY `intern_id` (`intern_id`);

--
-- Indexes for table `supervisor`
--
ALTER TABLE `supervisor`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attendance_sheet`
--
ALTER TABLE `attendance_sheet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=614;

--
-- AUTO_INCREMENT for table `document_uploads`
--
ALTER TABLE `document_uploads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=316;

--
-- AUTO_INCREMENT for table `feedback_form`
--
ALTER TABLE `feedback_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `hr_executives`
--
ALTER TABLE `hr_executives`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `interns`
--
ALTER TABLE `interns`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `onboarding_checklist`
--
ALTER TABLE `onboarding_checklist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `request_leave`
--
ALTER TABLE `request_leave`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT for table `supervisor`
--
ALTER TABLE `supervisor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `attendance_sheet`
--
ALTER TABLE `attendance_sheet`
  ADD CONSTRAINT `attendance_sheet_ibfk_1` FOREIGN KEY (`intern_id`) REFERENCES `interns` (`id`);

--
-- Constraints for table `document_uploads`
--
ALTER TABLE `document_uploads`
  ADD CONSTRAINT `document_uploads_ibfk_1` FOREIGN KEY (`intern_id`) REFERENCES `interns` (`id`);

--
-- Constraints for table `feedback_form`
--
ALTER TABLE `feedback_form`
  ADD CONSTRAINT `feedback_form_ibfk_1` FOREIGN KEY (`intern_id`) REFERENCES `interns` (`id`);

--
-- Constraints for table `interns`
--
ALTER TABLE `interns`
  ADD CONSTRAINT `interns_ibfk_1` FOREIGN KEY (`sv_id`) REFERENCES `supervisor` (`id`),
  ADD CONSTRAINT `interns_ibfk_2` FOREIGN KEY (`hr_id`) REFERENCES `hr_executives` (`id`);

--
-- Constraints for table `onboarding_checklist`
--
ALTER TABLE `onboarding_checklist`
  ADD CONSTRAINT `onboarding_checklist_ibfk_1` FOREIGN KEY (`intern_id`) REFERENCES `interns` (`id`);

--
-- Constraints for table `request_leave`
--
ALTER TABLE `request_leave`
  ADD CONSTRAINT `request_leave_ibfk_1` FOREIGN KEY (`intern_id`) REFERENCES `interns` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
