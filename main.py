"""
--- APP CONFIGURATION ---
"""
from flask import Flask
from flask import (
    render_template,
    redirect, request, session,
    url_for, flash, jsonify,
)

# MAIL CONFIGURATION
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication

# NEW EMAIL ADDRESS CONFIGURED - TRAVIS - 22/11/2021
EMAIL_ADDRESS = 'gitconnect.petronas@gmail.com'
EMAIL_PASSWORD = 'gitconnect12345'

from flask_wtf.csrf import CSRFProtect
from flask_bcrypt import Bcrypt
from functools import wraps
from datetime import datetime
import random
import string
import config as config #import the db configuration from config.py
import datetime


app = Flask(__name__)
app.config['SECRET_KEY'] = str(random.randint(1, 20))
bcrypt = Bcrypt(app)
csrf = CSRFProtect(app)

#MySQL configuration
from flask_mysqldb import MySQL, MySQLdb
app.config['MYSQL_HOST'] = config.host
app.config['MYSQL_PORT'] = 3306
app.config['MYSQL_USER'] = config.user
app.config['MYSQL_PASSWORD'] = config.password
app.config['MYSQL_DB'] = config.db_name
app.config['MYSQL_CURSORCLASS'] = 'DictCursor'
db = MySQL(app)

import os
THIS_FOLDER =  config.projPath + '/pip/static/resources' # change to your directory ..\resources
html_sample = os.path.join(THIS_FOLDER, 'email_template.html')
attendance_template = os.path.join(THIS_FOLDER, 'attendance_template.html')
app.config['FILE_UPLOADS'] = config.projPath + '/pip/static/uploads' # change to your director ..\uploads
app.config['ONBOARDING_UPLOADS'] = config.projPath + '/pip/static/uploads/onboarding' # change to your director ..\uploads
app.config['HR_UPLOADS'] = config.projPath + '/pip/static/uploads/hr' # change to your director ..\uploads

import pdfkit
# To use this package, need to set up the wkhtmltopdf in environment variable - .\wkhtmltopdf\bin
path_wkhtmltopdf = r'C:/Program Files/wkhtmltopdf/bin/wkhtmltopdf.exe'
config = pdfkit.configuration(wkhtmltopdf=path_wkhtmltopdf)

"""
--- APP LOGICS ---
"""
# --- HOME ---
@app.route('/')
def home():
    CurrentDate = datetime.datetime.now()
    return render_template('base.html', CurrentDate=CurrentDate)

# --- ALERT FOR APPROVED OR REJECT ---
@app.route('/status')
def status_alert():
    return render_template("status.html")

# --- INTERN LOGICS ---

@app.route('/intern/register', methods=['POST', 'GET'])
def intern_register():
    if request.method == 'POST':
        user_details = request.form
        """ Intern Details """
        name = user_details['name']
        email = user_details['email']
        password = user_details['password'].encode('utf-8')
        hashed_password = bcrypt.generate_password_hash(password)
        gender = user_details['gender']
        nationality = user_details['nationality']
        contact = user_details['contact']
        passport = user_details['passport']
        university = user_details['university']
        discipline = user_details['discipline']
        cgpa = user_details['cgpa']
        start_date = user_details['start_date']
        end_date = user_details['end_date']
        scholar = user_details['scholar']
        intern_dept = user_details['intern_dept']
        intern_div = user_details['intern_div']

        """ Supervisor Details """
        sv_name = user_details['sv_name']
        department = user_details['department']
        division = user_details['division']

        cursor = db.connection.cursor(MySQLdb.cursors.DictCursor)
        """Create supervisor detail"""
        cursor.execute("SELECT * FROM supervisor WHERE sv_name=%s", [sv_name])
        supervisor = cursor.fetchone()

        if not supervisor:
            cursor.execute("INSERT INTO supervisor (sv_name, department, division) VALUES (%s,%s,%s)",
                           [sv_name, department, division])
            db.connection.commit()

        """Check if the intern email existed"""
        cursor.execute('SELECT * FROM interns WHERE email = %s', [email])
        user = cursor.fetchone()

        if user:  # check if the user existed
            flash('The account for this is already existed. Please choose a different email', 'danger')
        else:  # create intern if the user not existed
            cursor.execute('SELECT * FROM supervisor WHERE sv_name = %s', [sv_name])
            supervisor = cursor.fetchone()
            cursor.execute(""" INSERT INTO interns (fullname, password, gender, nationality, email,
                           contact_no, nric, university, discipline, cgpa, start_date, end_date,
                           scholar, sv_id, intern_dept, intern_div)
                           VALUES (%s,%s, %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""",
                           [name, hashed_password, gender, nationality, email, contact, passport,
                           university, discipline, cgpa, start_date, end_date, scholar, supervisor['id'], intern_dept, intern_div])
            db.connection.commit()
            print("User created!")
            flash(f'Your details has been registered! You are now able to login', 'success')
            return redirect(url_for('intern_register'))
    return render_template('intern_register.html')


# Check if user logged in
def logged_in(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'loggedin' in session:
            return f(*args, **kwargs)
        else:
            flash('Please log in to access the lobby!', 'info')
            return redirect(url_for('intern_login', next=request.url))
    return wrap

@app.route('/login', methods=['POST', 'GET'])
def intern_login():
    if 'loggedin' in session:      # Direct user to home if logged in
        return redirect(url_for('home'))

    if request.method == 'POST' and 'username' in request.form and 'password' in request.form:
        user_details = request.form
        username = user_details['username']
        password = user_details['password']

        # Matching intern's username with the email in database
        cursor = db.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute('SELECT * FROM interns')
        interns = cursor.fetchall()

        email = ""
        for intern in interns:
            # If user logs in using personal email
            if intern['email'].startswith(username):
                email = intern['email']
            # If user logs in using petronas email
            elif intern['petronas_email'].startswith(username):
                email = intern['email']

        # Retrieved the data from matched email
        cursor.execute('SELECT * FROM interns WHERE email = %s', [email])
        user = cursor.fetchone()

        # Matching HR's username with the email in database
        cursor.execute('SELECT * FROM hr_executives WHERE hr_email = %s AND password = %s', (username, password,))
        hr = cursor.fetchone()

        if hr:
            # Create session data, we can access this data in other routes
            session['loggedin'] = True
            session['id'] = hr['id']
            session['username'] = hr['hr_email']
            session['fullname'] = hr['hr_name']
            # Redirect to home page
            print("Credentials checked!")
            next_page = request.args.get('next')
            return redirect(next_page) if next_page else redirect(url_for('staff_lobby'))

        if user and bcrypt.check_password_hash(user['password'], password):  # Check if the user login is exist
            session['loggedin'] = True
            session['id'] = user['id']
            session['email'] = user['email']
            session['fullname'] = user['fullname']
            session['end_date'] = user['end_date']
            session['end_date_final'] = datetime.datetime.strptime(session['end_date'], '%Y-%m-%d')
            print("Credentials checked!")

            """Update intern's last login"""
            cursor.execute("UPDATE interns SET last_login = NOW() WHERE id=%s", [session['id']])
            db.connection.commit()
            print('Intern Last Login Updated!')
            cursor.close()
            
            next_page = request.args.get('next')
            return redirect(next_page) if next_page else redirect(url_for('home'))
        else:
            flash('Login Unsuccessful. Please check Petronas ID and password', 'danger')
    return render_template('login.html')

@app.route('/intern/profile', methods=['POST', 'GET'])
@logged_in
def intern_profile():
    cursor = db.connection.cursor(MySQLdb.cursors.DictCursor) #connect to database

    """Retrieved intern details"""
    cursor.execute('SELECT * FROM interns WHERE id=%s', [session['id']])
    intern = cursor.fetchone()

    """Retrieved combined details; intern and supervisor"""
    if intern['hr_id'] is None:
        cursor.execute('SELECT * FROM interns i INNER JOIN supervisor s ON s.id = %s WHERE i.id=%s', [intern['sv_id'], session['id']])
        user = cursor.fetchone()
    else:
        cursor.execute('SELECT * FROM interns i INNER JOIN supervisor s ON s.id = %s INNER JOIN hr_executives h ON h.id = %s WHERE i.id=%s', [intern['sv_id'], intern['hr_id'], session['id']])
        user = cursor.fetchone()

    if request.method == 'POST':
        """ Getting the intern details from the form """
        user_details = request.form
        name = user_details['name']
        email = user_details['email']
        gender = user_details['gender']
        nationality = user_details['nationality']
        contact = user_details['contact']
        passport = user_details['passport']
        university = user_details['university']
        discipline = user_details['discipline']
        cgpa = user_details['cgpa']
        start_date = user_details['start_date']
        end_date = user_details['end_date']
        scholar = user_details['scholar']
        cost_centre = user_details['cost_centre']
        bank = user_details['bank']
        intern_dept = user_details['intern_dept']
        intern_div = user_details['intern_div']
        petronas_email = user_details['petronas_email']
        hr_additional_email = user_details['hr_additional_email']

        # Check if the email try to update is existed
        cursor.execute('SELECT email FROM interns')
        intern_emails = cursor.fetchall()

        for intern_email in intern_emails:
            if email == intern_email:
                flash('This email is already taken, please choose another', 'danger')
                return redirect(url_for('intern_profile'))

        """HR Details"""
        hr_email = user_details['hr_email']
        cursor.execute('SELECT * FROM hr_executives')
        hr_details = cursor.fetchall()

        # Check if the HR is existed
        for hr_detail in hr_details:
            if hr_detail['hr_email'] == hr_email:
                hr_id = hr_detail['id']

        """Update intern detail"""
        cursor.execute("""UPDATE interns SET fullname=%s, gender=%s, nationality=%s, email=%s, cost_centre=%s,
                        contact_no=%s, nric=%s, university=%s, discipline=%s, CGPA=%s, start_date=%s, end_date=%s, scholar=%s,
                        bank=%s, intern_dept=%s, intern_div=%s, hr_id=%s, petronas_email=%s, hr_additional_email=%s WHERE id=%s""",
                       [name, gender, nationality, email, cost_centre, contact, passport, university, discipline,
                        cgpa, start_date, end_date, scholar, bank, intern_dept, intern_div, hr_id, petronas_email, hr_additional_email,
                        session['id']])
        db.connection.commit()

        """ Supervisor Details """
        sv_name = user_details['sv_name']
        sv_email = user_details['sv_email']
        designation = user_details['designation']
        department = user_details['department']
        division = user_details['division']
        location = user_details['location']

        """ Update supervisor detail"""
        cursor.execute("UPDATE supervisor SET sv_name=%s, sv_email=%s, designation=%s, location=%s, department=%s, division=%s WHERE id=%s",
                       [sv_name, sv_email, designation, location, department, division, user['sv_id']])
        db.connection.commit()
        flash(f'Your profile is updated!', 'success')
        return redirect(url_for('intern_profile'))
    return render_template('intern_profile.html', user=user)

@app.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('home'))

""" --- ONBOARDING CHECKLIST LOGICS --- """
@app.route('/intern/onboarding_checklist', methods=['POST', 'GET'])
@logged_in
def onboarding():
    # Initialized item dictionary
    items = {'1': ' false', '2': 'false', '3': 'false', '4': 'false', '5': 'false', '6': 'false', '7': 'false', '8': 'false', '9': 'false', '10': 'false', '11': 'false', '12': 'false'}
    count1 = 0
    count2 = 0
    count3 = 0
    countCompletion = 0

    cursor = db.connection.cursor(MySQLdb.cursors.DictCursor)
    cursor.execute("SELECT * FROM onboarding_checklist WHERE intern_id = %s", [session['id']])
    user = cursor.fetchone()

    values = list(items.values())
    values.append(countCompletion)
    values.append(count1)
    values.append(count2)
    values.append(count3)

    if user is None:
        values.append(session['id'])
        """Create onboarding checklist"""
        cursor.execute("INSERT INTO onboarding_checklist (item_1, item_2, item_3, item_4, item_5, item_6, item_7, item_8, item_9, item_10, item_11, item_12, completion, count1, count2, count3, intern_id) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
                       values)
        db.connection.commit()
        return redirect(url_for('onboarding'))

    if request.method == 'POST':
        checkbox_items = request.form.getlist('item')
        # Replaced retrieved data from checkbox input to 'true' in dict if checked
        for checkbox_item in checkbox_items:
            for keys in list(items.keys()):
                if checkbox_item == keys:
                    items[checkbox_item] = 'true'

        # Calculation for the total mark of checked items and completion
        for key, value in items.items():
            if key == '1' and value == 'true' or key == '2' and value == 'true':
                count1 += 1
                countCompletion += 5
            elif key == '3' and value == 'true' or key == '4' and value == 'true' or key == '5' and value == 'true' or key == '6' and value == 'true' or key == '7' and value == 'true' or key == '8' and value == 'true':
                count2 += 1
                countCompletion += 10
            elif key == '9' and value == 'true' or key == '10' and value == 'true' or key == '11' and value == 'true' or key == '12' and value == 'true':
                count3 += 1
                countCompletion += 7.5

        list_items = list(items.values())
        list_items.append(countCompletion)
        list_items.append(count1)
        list_items.append(count2)
        list_items.append(count3)

        if user:
            list_items.append(user['id'])
            cursor.execute("UPDATE onboarding_checklist SET item_1=%s, item_2=%s, item_3=%s, item_4=%s, item_5=%s, item_6=%s, item_7=%s, item_8=%s, item_9=%s, item_10=%s, item_11=%s, item_12=%s, completion=%s, count1=%s, count2=%s, count3=%s WHERE id=%s",
                           list_items)
            db.connection.commit()
        flash('Checklist is successfully saved!', 'success')
        return redirect(url_for('onboarding'))
    return render_template('onboarding.html', item=user)


@app.route('/intern/onboarding_checklist/upload<int:count>', methods=['POST', 'GET'])
@logged_in
def onboarding_upload(count):
    cursor = db.connection.cursor(MySQLdb.cursors.DictCursor)

    """Retrieved intern details"""
    cursor.execute('SELECT * FROM interns WHERE id=%s', [session['id']])
    intern = cursor.fetchone()

    document_dict = {'secrecy_agreement': 1, 'indemnity_letter': 2, 'insurance_letter': 3, 'offer_letter': 4, 'copy_nric': 5, 'bank_statement': 6}

    if request.method == "POST":
        file = request.files['intern_file']

        for key, value in document_dict.items():
            if value == count:
                if value == 1:
                    filename = f'{intern["fullname"]}_Secrecy Agreement_{intern["id"]}.pdf' #set filename
                    file.save(os.path.join(app.config['ONBOARDING_UPLOADS'], filename))
                    cursor.execute("INSERT INTO document_uploads (type, filename, data, intern_id) VALUES (%s, %s,%s,%s)", [key, filename, file.read(), session['id']])
                    db.connection.commit()

                elif value == 2:
                    filename = f'{intern["fullname"]}_Indemnity Letter_{intern["id"]}.pdf' #set filename
                    file.save(os.path.join(app.config['ONBOARDING_UPLOADS'], filename))
                    cursor.execute("INSERT INTO document_uploads (type, filename, data, intern_id) VALUES (%s, %s,%s,%s)", [key, filename, file.read(), session['id']])
                    db.connection.commit()

                elif value == 3:
                    filename = f'{intern["fullname"]}_Insurance Letter_{intern["id"]}.pdf' #set filename
                    file.save(os.path.join(app.config['ONBOARDING_UPLOADS'], filename))
                    cursor.execute("INSERT INTO document_uploads (type, filename, data, intern_id) VALUES (%s, %s,%s,%s)", [key, filename, file.read(), session['id']])
                    db.connection.commit()

                elif value == 4:
                    filename = f'{intern["fullname"]}_Offer Letter_{intern["id"]}.pdf' #set filename
                    file.save(os.path.join(app.config['ONBOARDING_UPLOADS'], filename))
                    cursor.execute("INSERT INTO document_uploads (type, filename, data, intern_id) VALUES (%s, %s,%s,%s)", [key, filename, file.read(), session['id']])
                    db.connection.commit()

                elif value == 5:
                    filename = f'{intern["fullname"]}_Copy of IC-Passport_{intern["id"]}.pdf' #set filename
                    file.save(os.path.join(app.config['ONBOARDING_UPLOADS'], filename))
                    cursor.execute("INSERT INTO document_uploads (type, filename, data, intern_id) VALUES (%s, %s,%s,%s)", [key, filename, file.read(), session['id']])
                    db.connection.commit()

                elif value == 6:
                    filename = f'{intern["fullname"]}_Bank Statement_{intern["id"]}.pdf' #set filename
                    file.save(os.path.join(app.config['ONBOARDING_UPLOADS'], filename))
                    cursor.execute("INSERT INTO document_uploads (type, filename, data, intern_id) VALUES (%s, %s,%s,%s)", [key, filename, file.read(), session['id']])
                    db.connection.commit()

                count += 2
                value = 'true'
                cursor.execute(f"UPDATE onboarding_checklist SET item_{ count }=%s WHERE intern_id=%s", [value, session['id']])
                db.connection.commit()

                print(f'{ key } is uploaded!')
                flash(f'File has uploaded!', 'success')
                return redirect(url_for('onboarding'))


@app.route('/intern/onboarding_checklist/submit', methods=['POST', 'GET'])
@logged_in
def onboarding_submit():
    cursor = db.connection.cursor(MySQLdb.cursors.DictCursor)

    """Retrieved intern details"""
    cursor.execute('SELECT * FROM interns WHERE id=%s', [session['id']])
    intern = cursor.fetchone()

    cursor.execute("SELECT * FROM onboarding_checklist WHERE intern_id = %s", [session['id']])
    checklist = cursor.fetchone()

    cursor.execute('SELECT * FROM interns i INNER JOIN supervisor s ON s.id = %s INNER JOIN hr_executives h ON h.id = %s WHERE i.id=%s', [intern['sv_id'], intern['hr_id'], session['id']])
    user = cursor.fetchone()


    if request.method == 'POST':
        remarks = request.form.get('remarks')

        data = {}
        content = render_template('emailcontents/onboarding_content.html', detail=user, checklist=checklist, remarks=remarks)

        recipients = [user['hr_email'], intern['hr_additional_email']]

        data["subject"] = f"{user['fullname']} has completed the onboarding checklist!"
        data["recipient"] = data["recipient"] = ','.join(map(str, recipients))
        data["body"] = content

        # If No Remarks Included (Full Submission of Documents)
        if remarks is None:
            send_email(data)
            print('Sent checklist without remarks')
            flash('Successfully sent checklist!', 'success')
            checklist_status = 'submitted'
            cursor = db.connection.cursor(MySQLdb.cursors.DictCursor)
            cursor.execute("UPDATE interns SET checklist_status=%s WHERE id=%s", [checklist_status, session['id']])
            db.connection.commit()
            print('Updated status!')

        # If Remarks Included (Not Full Submission of Documents)
        else:
            cursor.execute("INSERT INTO onboarding_checklist (remarks) VALUES (%s)", [remarks])
            db.connection.commit()
            send_email(data)
            print('Sent checklist with remarks')
            flash('Successfully sent checklist!', 'success')
            checklist_status = 'submitted'
            cursor = db.connection.cursor(MySQLdb.cursors.DictCursor)
            cursor.execute("UPDATE interns SET checklist_status=%s WHERE id=%s", [checklist_status, session['id']])
            db.connection.commit()
            print('Updated status!')

        return redirect(url_for('onboarding'))


""" --- ATTENDANCE SHEET LOGICS --- """
@app.route('/intern/attendance')
@logged_in
def attendance():
    cursor = db.connection.cursor(MySQLdb.cursors.DictCursor)
    cursor.execute('SELECT * FROM attendance_sheet WHERE intern_id = %s', [session['id']]) # Shows all the attendancesheets
    print("Retrieved the data")
    attendance = cursor.fetchall()
    return render_template('attendance.html', attendance=attendance)

@app.route('/intern/attendance/<int:id>/editor', methods=['POST', 'GET'])
@logged_in
def attendance_editor(id):
    data = {}
    year = datetime.datetime.now().year
    # Get the HTML template from local folder to display on the editor
    contents = [attendance_template]
    for content in contents:
        with open(content, 'rb') as f_content:
            html_data = f_content.read()
            html_string = html_data.decode(encoding='UTF-8')

    cursor = db.connection.cursor(MySQLdb.cursors.DictCursor)
    cursor.execute('SELECT * FROM attendance_sheet WHERE id = %s', [id])
    print("Retrieved the attendance sheet")
    attendance = cursor.fetchone()

    #Retrieved intern data
    cursor.execute('SELECT * FROM interns WHERE id = %s', [session['id']])
    intern = cursor.fetchone()

    cursor.execute('SELECT * FROM interns i INNER JOIN attendance_sheet a ON a.intern_id = %s INNER JOIN supervisor s ON s.id = %s WHERE i.id=%s', [intern['id'], intern['sv_id'], session['id']])
    print("Retrieved the data from intern")
    user = cursor.fetchone()

    # Retrieved the data from template
    if request.method == "POST":
        name = request.form.get('name')
        petronas_scholar = request.form.get('scholar')
        department = request.form.get('department')
        division = request.form.get('division')
        sv_email = request.form.get('sv_email')
        date = request.form.get('date')
        content = request.form.get('content')

        data['name'] = name
        data['petronas_scholar'] = petronas_scholar
        data['division'] = division
        data['department'] = department
        data['sv_email'] = sv_email
        data['date'] = date
        data['content'] = content

        status = 'inprogress'
        date = datetime.datetime.now()

        rendered_content = render_template('content.html', data=data)

        # create a hashed password using sv name as the digital signature using flask-bcrypt
        signature = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(8))

        cursor.execute("UPDATE attendance_sheet SET html_file=%s, status=%s, intern_sign=%s, last_modified=%s WHERE id=%s", [str(rendered_content), status, signature, date, id])
        db.connection.commit()
        print('Saved content!')

        """ Generate attendance sheet pdf and save to local folder """
        rendered = render_template('attendance_sheet.html', data=data, attendance=attendance, detail=user, signature=signature, year=year)
        generate_pdf = pdfkit.from_string(rendered, False, configuration=config)

        # Save pdf to the local folder
        filename = f'{intern["fullname"]}_{attendance["attendance_month"]}_{year}.pdf' #set filename
        folder = os.path.join(app.config['FILE_UPLOADS'], 'attendance')  # join the filename with path

        if not os.path.isdir(folder):
            os.mkdir(folder)

        path = os.path.join(folder, filename)
        f = open(path, "wb")
        f.write(generate_pdf)
        print("File saved!")
        f.close()

        flash(f'Attendance Sheet for {attendance["attendance_month"]} is Updated!', 'success')

        return redirect(url_for('attendance'))

    return render_template('attendance_editor.html', html_data=html_string, attendance=attendance, user=user)

@app.route('/intern/attendance/<int:id>/approval', methods=['POST', 'GET'])
@logged_in
def attendance_approval(id):
    status = 'approval'
    date = datetime.datetime.now().date()
    year = datetime.datetime.now().year

    cursor = db.connection.cursor(MySQLdb.cursors.DictCursor)
    cursor.execute("UPDATE attendance_sheet SET status=%s, approved_date=%s WHERE id=%s", [status, date, id])
    db.connection.commit()
    print('Updated status!')

    """Retrieved intern details"""
    cursor.execute('SELECT * FROM interns WHERE id=%s', [session['id']])
    intern = cursor.fetchone()

    cursor.execute('SELECT * FROM attendance_sheet WHERE id=%s', [id])
    attendance = cursor.fetchone()

    cursor.execute('SELECT * FROM interns i INNER JOIN supervisor s ON s.id = %s INNER JOIN hr_executives h ON h.id = %s WHERE i.id=%s',[intern['sv_id'], intern['hr_id'], session['id']])
    user = cursor.fetchone()

    # Send email to supervisor
    data = {}
    content = render_template('emailcontents/attendance_content.html', detail=user, attendance=attendance, year=year)

    data["subject"] = f"Attendance Sheet Submission - { attendance['attendance_month'] } { year }"
    data["recipient"] = user['sv_email']
    data["body"] = content
    data["type"] = 'attendance'
    data["id"] = id
    send_email(data)
    flash('Email sent successfully to supervisor!', 'success')

    return redirect(url_for('attendance'))

@app.route('/intern/attendance/<int:id>/<int:intern_id>/approval/<status>', methods=['POST', 'GET'])
def attendance_approval_sign(id, intern_id, status):
    date = datetime.datetime.now().date()
    year = datetime.datetime.now().year

    # retrieved the intern, supervisor data from database
    cursor = db.connection.cursor(MySQLdb.cursors.DictCursor)
    cursor.execute("UPDATE attendance_sheet SET status=%s, approved_date=%s WHERE id=%s", [status, date, id])
    db.connection.commit()
    print('Updated status!')

    """Retrieved intern details"""
    cursor.execute('SELECT * FROM interns WHERE id=%s', [intern_id])
    intern = cursor.fetchone()

    cursor.execute('SELECT * FROM attendance_sheet WHERE id=%s', [id])
    attendance = cursor.fetchone()

    cursor.execute('SELECT * FROM interns i INNER JOIN supervisor s ON s.id = %s INNER JOIN hr_executives h ON h.id = %s WHERE i.id = %s', [intern['sv_id'], intern['hr_id'], session['id']])
    user = cursor.fetchone()

    # create a hashed password using sv name as the digital signature using flask-bcrypt
    sv_signature = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(8))

    # store the signature in database
    cursor.execute("UPDATE attendance_sheet SET sv_sign=%s WHERE id=%s",
                   [sv_signature, id])
    db.connection.commit()

    # render template of html with digital signature parameter and generate to pdf
    rendered = render_template('attendance_sheet.html', detail=user, attendance=attendance, sv_signature=sv_signature, year=year)
    generate_pdf = pdfkit.from_string(rendered, False, configuration=config)

    filename = f'{intern["fullname"]}_{attendance["attendance_month"]}_{year}.pdf'  # set filename
    folder = os.path.join(app.config['FILE_UPLOADS'], 'attendance')  # join the filename with path

    path = os.path.join(folder, filename)
    f = open(path, "wb")
    f.write(generate_pdf)
    print("File saved!")
    f.close()

    # send email to the intern back for further action
    data = {}
    content = render_template('emailcontents/attendance_status.html', detail=user, attendance=attendance, status=status)

    data["subject"] = f"Attendance Sheet Submission Status - { attendance['attendance_month'] }"
    data["recipient"] = user['email']
    data["body"] = content
    send_email(data)

    if status == 'approved':
        flash(f'You have { status } the attendance sheet', 'success')
    else:
        flash(f'You have {status} the attendance sheet', 'danger')

    return redirect(url_for('status_alert'))

@app.route('/intern/attendance/<int:id>/submit', methods=['POST', 'GET'])
@logged_in
def attendance_submit(id):
    status = 'complete'
    date = datetime.datetime.now().date()
    year = datetime.datetime.now().year

    cursor = db.connection.cursor(MySQLdb.cursors.DictCursor)
    cursor.execute("UPDATE attendance_sheet SET status=%s, submitted_date=%s WHERE id=%s", [status, date, id])
    db.connection.commit()
    print('Updated status!')

    """Retrieved intern details"""
    cursor.execute('SELECT * FROM interns WHERE id=%s', [session['id']])
    intern = cursor.fetchone()

    cursor.execute('SELECT * FROM attendance_sheet WHERE id=%s', [id])
    attendance = cursor.fetchone()

    cursor.execute('SELECT * FROM interns i INNER JOIN supervisor s ON s.id = %s INNER JOIN hr_executives h ON h.id = %s WHERE i.id=%s', [intern['sv_id'], intern['hr_id'], session['id']])
    user = cursor.fetchone()

    # Save pdf to the local folder
    filename = f'{intern["fullname"]}_{attendance["attendance_month"]}_{year}.pdf' #set filename
    cursor.execute("UPDATE attendance_sheet SET filename=%s WHERE id=%s",
                   [filename, id])
    db.connection.commit()

    # Update the pdf file in attendance sheet database

    # Send email to HR
    data = {}
    content = render_template('emailcontents/attendance_submit.html', detail=user, attendance=attendance, year=year)

    recipients = [user['hr_email'], intern['hr_additional_email']]

    data["subject"] = f"Attendance Sheet Submission - { attendance['attendance_month'] } { year }"
    data["recipient"] = ','.join(map(str, recipients))
    data["body"] = content
    data["type"] = 'attendance'
    data["id"] = id
    send_email(data)
    flash('Email sent successfully to your HR focal!', 'success')

    return redirect(url_for('attendance'))

@app.route('/intern/attendance/add', methods=['POST', 'GET'])
@logged_in
def attendance_add():
    if request.method == 'POST':
        month = request.form.get('month')

        cursor = db.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute('SELECT * FROM attendance_sheet WHERE intern_id = %s', [session['id']])
        attendance = cursor.fetchall()

        if attendance is None:
            cursor.execute('INSERT INTO attendance_sheet (intern_id) VALUES (%s)', [session['id']])

        # Check if the month attendance sheet already existed
        existed_months = []
        for item in attendance:
            existed_months.append(item['attendance_month'])

        if month in existed_months:
            flash(f'{ month } attendance sheet is already created! Please choose another month', 'danger')
            return redirect(url_for('attendance'))
        else:
            cursor.execute("INSERT INTO attendance_sheet (attendance_month, intern_id) VALUES (%s,%s)", [month, session['id']])
            db.connection.commit()
            flash(f'{ month } attendance sheet is created!', 'success')
            return redirect(url_for('attendance'))

# Delete Attendance Function

@app.route('/intern/attendance/<int:id>/delete', methods=['POST', 'GET'])
@logged_in
def attendance_delete(id):
        cursor = db.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute('DELETE FROM attendance_sheet WHERE id = %s', [id])
        db.connection.commit()
        print("Retrieved the attendance sheet")
        flash('Attendance sheet has been successfully deleted!', 'success')
        return redirect(url_for('attendance'))

""" --- REQUEST FOR LEAVE LOGICS --- """
@app.route('/intern/request_for_leave', methods=['POST', 'GET'])
@logged_in
def leave_request():
    data = {}
    date = datetime.datetime.now().date()

    cursor = db.connection.cursor(MySQLdb.cursors.DictCursor)
    # Retrieved intern data
    cursor.execute('SELECT * FROM interns WHERE id=%s', [session['id']])
    intern = cursor.fetchone()

    # Retrieved latest leave request data
    cursor.execute('SELECT * FROM request_leave WHERE intern_id=%s ORDER BY id DESC LIMIT 1', [session['id']])
    leave = cursor.fetchone()

    # Combine the table intern, supervisor, hr
    cursor.execute('SELECT * FROM interns i INNER JOIN supervisor s ON s.id = %s INNER JOIN hr_executives h ON h.id = %s WHERE i.id=%s', [intern['sv_id'], intern['hr_id'], session['id']])
    user = cursor.fetchone()

    if request.method == 'POST':
        data['date'] = request.form.get('date')
        data['duration'] = request.form.get('duration')
        data['name'] = request.form.get('name')
        data['relation'] = request.form.get('relation')
        data['address'] = request.form.get('address')
        data['contact'] = request.form.get('contact')
        data['program'] = request.form.get('program')
        data['justification'] = request.form.get('justification')
        uploaded_files = request.files.getlist("files")

        data_list = list(data.values())

        #Generate signature for intern
        intern_sign = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(8))

        data_list.append(intern_sign)
        data_list.append(session['id'])

        #Insert leave request details into database
        cursor.execute("""INSERT INTO request_leave (selected_date, duration, emergency_name, relation, address, contact, type_program, justification, intern_sign, intern_id)
                        VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""",
                       data_list)
        db.connection.commit()

        if not uploaded_files == " ":
            for uploaded_file in uploaded_files:
                uploaded_file.save(os.path.join(app.config['ONBOARDING_UPLOADS'], uploaded_file.filename))
                type = "leave_request"
                cursor.execute("INSERT INTO document_uploads (type, filename, data, intern_id) VALUES (%s,%s,%s,%s)",
                               [type, uploaded_file.filename, uploaded_file.read(), session['id']])
                db.connection.commit()
                print("Leave Request File Successfully Uploaded!")

        # Retrieved latest leave request data
        cursor.execute('SELECT * FROM request_leave WHERE intern_id=%s ORDER BY id DESC LIMIT 1', [session['id']])
        leave_latest = cursor.fetchone()

        """ Generate leave request form pdf and save to local folder """
        rendered = render_template('leave_form.html', data=leave_latest, detail=user, date=date)
        generate_pdf = pdfkit.from_string(rendered, False, configuration=config)

        filename = f'{intern["fullname"]}_Leave Request_{ leave_latest["type_program"] }_{ leave_latest["id"] }.pdf'  # set filename
        folder = os.path.join(app.config['FILE_UPLOADS'], 'leave')  # join the filename with path
        type = "leave"
        cursor.execute("INSERT INTO document_uploads (type, filename, intern_id) VALUES (%s,%s,%s)",
                      [type, filename, session['id']])
        db.connection.commit()

        if not os.path.isdir(folder):
            os.mkdir(folder)

        path = os.path.join(folder, filename)
        f = open(path, "wb")
        f.write(generate_pdf)
        print("File saved!")
        f.close()

        """ Send email to supervisor """
        email_data = {}

        content = render_template('emailcontents/leave_content.html', leave=leave_latest, data=data, detail=user)
        email_data["subject"] = f"{ user['fullname'] } - Request for Leave (Supervisor Approval)"
        email_data["recipient"] = user['sv_email']
        email_data["body"] = content
        email_data["type"] = 'leave'
        email_data["intern_id"] = intern["id"]
        send_email(email_data)
        flash('Request for leave sent successfully to the supervisor for approval!', 'success')
        return redirect(url_for('leave_request'))
    return render_template('leave_request.html', leave=leave)

@app.route('/intern/request_for_leave/<int:id>/sv_approval/<status>', methods=['POST', 'GET'])
def leave_request_supervisor(id, status):

    date = datetime.datetime.now().date()

    cursor = db.connection.cursor(MySQLdb.cursors.DictCursor)

    # Retrieved intern data
    cursor.execute('SELECT * FROM interns WHERE id=%s', [id])
    intern = cursor.fetchone()

    # Retrieved latest leave request data
    cursor.execute('SELECT * FROM request_leave WHERE intern_id=%s ORDER BY id DESC LIMIT 1', [id])
    leave = cursor.fetchone()

    # Combine the table intern, supervisor, hr
    cursor.execute('SELECT * FROM interns i INNER JOIN supervisor s ON s.id = %s INNER JOIN hr_executives h ON h.id = %s WHERE i.id=%s', [intern['sv_id'], intern['hr_id'], id])
    user = cursor.fetchone()

    # Generate sv signature
    sv_sign = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(8))

    # Update the signature in database
    cursor.execute("UPDATE request_leave SET sv_sign=%s WHERE intern_id=%s", [sv_sign, id])
    db.connection.commit()

    # send email to HR
    email_data = {}

    # If SV approves leave request
    if status == "approved":

        # Update SV Approval Date
        cursor.execute("UPDATE request_leave SET sv_approval_date = NOW() WHERE id=%s", [leave["id"]])
        db.connection.commit()

        # Update SV Approval to Approved
        cursor.execute("UPDATE request_leave SET sv_approval = 'Approved' WHERE id=%s", [leave["id"]])
        db.connection.commit()

        # Selecting the Approved Attendance Sheet
        cursor.execute('SELECT * FROM request_leave WHERE id=%s', [leave["id"]])
        leave_latest = cursor.fetchone()

        # Update the PDF
        rendered = render_template('leave_form.html', data=leave_latest, detail=user, date=date)
        generate_pdf = pdfkit.from_string(rendered, False, configuration=config)

        # Setting up filename
        filename = f'{intern["fullname"]}_Leave Request_{ leave["type_program"] }_{ leave["id"] }.pdf'  

        # Joining the filename with path
        folder = os.path.join(app.config['FILE_UPLOADS'], 'leave')

        path = os.path.join(folder, filename)
        f = open(path, "wb")
        f.write(generate_pdf)
        print("File updated!")
        f.close()

        recipients = [user['hr_email'], intern['hr_additional_email']]

        content = render_template('emailcontents/leave_content_hr.html', data=leave, detail=user)
        email_data["subject"] = f"{user['fullname']} - Request for Leave (HOD Approval)"
        email_data["recipient"] = ','.join(map(str, recipients))
        email_data["body"] = content
        email_data["type"] = 'leave'
        email_data["intern_id"] = intern["id"]
        send_email(email_data)
        flash('Email sent successfully to the head of department!', 'success')
        return redirect(url_for('status_alert'))

    # If SV rejects leave request
    else:

        # Update SV Approval Date
        cursor.execute("UPDATE request_leave SET sv_approval_date = NOW() WHERE id=%s", [leave["id"]])
        db.connection.commit()

        # Update SV Approval to Reject
        cursor.execute("UPDATE request_leave SET sv_approval = 'Rejected' WHERE id=%s", [leave["id"]])
        db.connection.commit()

        # Selecting the Rejected Attendance Sheet
        cursor.execute('SELECT * FROM request_leave WHERE id=%s', [leave["id"]])
        leave_latest = cursor.fetchone()

        # Update the PDF
        rendered = render_template('leave_form.html', data=leave_latest, detail=user, date=date)
        generate_pdf = pdfkit.from_string(rendered, False, configuration=config)

        # Setting up filename
        filename = f'{intern["fullname"]}_Leave Request_{ leave["type_program"] }_{ leave["id"] }.pdf' 

        # Joining the filename with path
        folder = os.path.join(app.config['FILE_UPLOADS'], 'leave') 

        path = os.path.join(folder, filename)
        f = open(path, "wb")
        f.write(generate_pdf)
        print("File updated!")
        f.close()

        content = render_template('emailcontents/leave_content_reject.html', data=leave, detail=user)
        email_data["subject"] = f"Request for Leave Status"
        email_data["recipient"] = user['email']
        email_data["body"] = content
        send_email(email_data)
        flash('Email sent successfully to the intern!', 'success')
        return redirect(url_for('status_alert'))


# @app.route('/intern/request_for_leave/<int:id>/hod/<status>', methods=['POST', 'GET'])
# def leave_request_hod(id, status):

#     date = datetime.datetime.now().date()
#     cursor = db.connection.cursor(MySQLdb.cursors.DictCursor)

#     # Retrieved intern data
#     cursor.execute('SELECT * FROM interns WHERE id=%s', [id])
#     intern = cursor.fetchone()

#     # Retrieved latest leave request data
#     cursor.execute('SELECT * FROM request_leave WHERE intern_id=%s ORDER BY id DESC LIMIT 1', [id])
#     leave = cursor.fetchone()

#     # Combine the table intern, supervisor, hr
#     cursor.execute('SELECT * FROM interns i INNER JOIN supervisor s ON s.id = %s INNER JOIN hr_executives h ON h.id = %s WHERE i.id=%s', [intern['sv_id'], intern['hr_id'], id])
#     user = cursor.fetchone()

#     # Generate hod signature
#     hod_sign = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(8))

#     # Update the signature in database
#     cursor.execute("UPDATE request_leave SET hod_sign=%s WHERE intern_id=%s", [hod_sign, id])
#     db.connection.commit()

#     # send email to HR
#     email_data = {}

#     recipients = [user['hr_email'], intern['hr_additional_email']]

#     # if HOD approves the leave request
#     if status == "approved":

#         # Update HOD Approval Date
#         cursor.execute("UPDATE request_leave SET hod_approval_date = NOW() WHERE id=%s", [leave["id"]])
#         db.connection.commit()

#         # Update HOD Approval to Approved
#         cursor.execute("UPDATE request_leave SET hod_approval = 'Approved' WHERE id=%s", [leave["id"]])
#         db.connection.commit()

#         # Selecting the Approved Attendance Sheet
#         cursor.execute('SELECT * FROM request_leave WHERE id=%s', [leave["id"]])
#         leave_latest = cursor.fetchone()

#         #Update PDF
#         rendered = render_template('leave_form.html', data=leave_latest, detail=user, date=date)
#         generate_pdf = pdfkit.from_string(rendered, False, configuration=config)

#         filename = f'{intern["fullname"]}_Leave Request_{ leave["type_program"] }_{ leave["id"] }.pdf'  # set filename
#         folder = os.path.join(app.config['FILE_UPLOADS'], 'leave')  # join the filename with path

#         path = os.path.join(folder, filename)
#         f = open(path, "wb")
#         f.write(generate_pdf)
#         print("File updated!")
#         f.close()

#         content = render_template('emailcontents/leave_content_hr.html', data=leave, detail=user)
#         email_data["subject"] = f"{user['fullname']} - Request for Leave"
#         email_data["recipient"] = ','.join(map(str, recipients)) # + ',' + user['hod_email'] #please change to HOD email
#         email_data["body"] = content
#         email_data["type"] = 'leave'
#         email_data["intern_id"] = intern["id"]
#         send_email(email_data)
#         flash('Email sent successfully to the HR focal!', 'success')
#         return redirect(url_for('status_alert'))

#     # If HOD rejects the leave request
#     else:
#         content = render_template('emailcontents/leave_content_reject.html', data=leave, detail=user)
#         email_data["subject"] = f"Request for Leave Status"
#         email_data["recipient"] = user['email']
#         email_data["body"] = content
#         send_email(email_data)
#         flash('Email sent successfully to the intern!', 'success')
#         return redirect(url_for('status_alert'))

@app.route('/intern/request_for_leave/approval', methods=['POST', 'GET'])
@logged_in
def leave_request_status(id, status):
    return render_template('leave_request_status.html')


@app.route('/intern/feedback_form', methods=['POST', 'GET'])
@logged_in
def feedback():
    item_dict = {}
    item_list = []
    year = datetime.datetime.now().year
    cursor = db.connection.cursor(MySQLdb.cursors.DictCursor)
    cursor.execute("SELECT * FROM feedback_form WHERE intern_id=%s", [session['id']])
    user = cursor.fetchone()

    cursor.execute('SELECT * FROM interns WHERE id=%s', [session['id']])
    interns = cursor.fetchone()

    cursor.execute('SELECT * FROM interns i INNER JOIN supervisor s ON s.id = %s INNER JOIN hr_executives h ON h.id = %s WHERE i.id=%s', [interns['sv_id'], interns['hr_id'], session['id']])
    intern = cursor.fetchone()

    if request.method == 'POST':
        for i in range(1, 15):
            item_list.append(f'item{i}')

        for item in item_list:
            item_dict[item] = request.form.get(item)

        item_values = list(item_dict.values())

        # Check if the intern has completed the feedback form
        if user:
            flash('Intern has already completed the feedback form', 'danger')
            return redirect(url_for('feedback'))
        else:
            item_values.append(session['id'])
            cursor.execute("""INSERT INTO feedback_form (item1, item2, item3, item4, item5, item6, item7, item8, item9, item10, item11, item12, item13, item14, intern_id)
                            VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""", item_values)
            db.connection.commit()
            
        rendered = render_template('feedback_form.html', detail=intern, feedback=item_dict)
        generate_pdf = pdfkit.from_string(rendered, False, configuration=config)

        filename = f'{intern["fullname"]}_Feedback Form_{year}.pdf'  # set filename
        folder = os.path.join(app.config['FILE_UPLOADS'], 'feedback')  # join the filename with path
        type = "feedback"
        cursor.execute("INSERT INTO document_uploads (type, filename, intern_id) VALUES (%s,%s,%s)",
                      [type, filename, session['id']])
        db.connection.commit()

        if not os.path.isdir(folder):
            os.mkdir(folder)

        path = os.path.join(folder, filename)
        f = open(path, "wb")
        f.write(generate_pdf)
        print("File saved!")
        f.close()

        """ Send email to supervisor """
        email_data = {}
        content = render_template('emailcontents/feedback_content.html', detail=intern)

        recipients = [intern['hr_email'], interns['hr_additional_email']]

        email_data["subject"] = f"{intern['fullname']} - Feedback Form"
        email_data["recipient"] = ','.join(map(str, recipients))
        email_data["body"] = content
        email_data["type"] = 'feedback'
        email_data["intern_id"] = intern["id"]
        send_email(email_data)
        flash('Feedback form sent successfully to the HR focal!', 'success')
        return redirect(url_for('feedback'))

    return render_template('feedback.html')

# --- STAFF LOGICS ---

# Check if user logged in
@app.route('/staff')
@logged_in
def staff_lobby():
    return render_template('staff.html')

@app.route('/staff/profile', methods=['POST', 'GET'])
@logged_in
def staff_profile():
    cursor = db.connection.cursor(MySQLdb.cursors.DictCursor) #connect to database

    """Retrieved HR details"""
    cursor.execute('SELECT * FROM hr_executives WHERE id=%s', [session['id']])
    user = cursor.fetchone()

    if request.method == 'POST':
        """ Getting the HR details from the form """
        user_details = request.form
        hr_name = user_details['hr_name']
        hr_email = user_details['hr_email']
        location = user_details['location']
        department = user_details['department']
        division = user_details['division']
        register_at = user_details['register_at']
        last_login = user_details['last_login']

        """Update HR detail"""
        cursor.execute("""UPDATE hr_executives SET hr_name=%s, hr_email=%s, location=%s, department=%s, division=%s,
                       register_at=%s, last_login=%s WHERE id=%s""",
                       [hr_name, hr_email, location, department, division, register_at, last_login, session['id']])
        db.connection.commit()
        flash(f'Your profile is updated!', 'success')
        return redirect(url_for('staff_profile'))
    return render_template('staff_profile.html', user=user)

@app.route('/staff/checklist_sub')
@logged_in
def checklist_submission():
    return render_template('staff_checklistsub.html')

@app.route('/staff/submission_status')
@logged_in
def submission_status():
    cursor = db.connection.cursor(MySQLdb.cursors.DictCursor) #connect to database

    """Retrieved intern details"""
    cursor.execute('SELECT * FROM interns')
    data = cursor.fetchall()

    # Getting Current Date
    CurrentDate = datetime.datetime.now()

    return render_template('submission_status.html', interns=data, CurrentDate=CurrentDate)

@app.route('/staff/submission_status/edit/<id>', methods=['POST', 'GET'])
@logged_in
def submission_status_edit(id):
    cursor = db.connection.cursor(MySQLdb.cursors.DictCursor) #connect to database

    cursor.execute('SELECT * FROM interns WHERE id=%s', [id])
    data = cursor.fetchone()

    cursor.execute('SELECT * FROM interns i INNER JOIN supervisor s ON s.id = %s WHERE i.id=%s', [data['sv_id'], [id]])
    data = cursor.fetchone()

    return render_template('submission_status_edit.html', interns=data)

@app.route('/staff/submission_status/update/<id>', methods=['POST', 'GET'])
@logged_in
def submission_status_update(id):

    cursor = db.connection.cursor(MySQLdb.cursors.DictCursor) #connect to database

    """Retrieved intern details"""
    cursor.execute('SELECT * FROM interns WHERE id=%s', [id])
    interns = cursor.fetchall()

    if request.method == 'POST':

        """ Getting the intern details from the form """
        user_details = request.form
        name = user_details['name']
        email = user_details['email']
        contact = user_details['contact']
        intern_dept = user_details['intern_dept']
        intern_div = user_details['intern_div']
        start_date = user_details['start_date']
        end_date = user_details['end_date']
        cost_centre = user_details['cost_centre']

        """Update intern detail"""
        cursor.execute("""UPDATE interns SET fullname=%s, email=%s, contact_no=%s, intern_dept=%s, intern_div=%s, start_date=%s, end_date=%s, cost_centre=%s
                        WHERE id=%s""",
                        [name, email, contact, intern_dept, intern_div, start_date, end_date, cost_centre, id])

        db.connection.commit()
        flash('Successfully update profile!', 'success')
        return redirect(url_for('submission_status'))
    return render_template('submission_status_edit.html', interns=interns[0])


@app.route('/staff/submission_status/filter', methods=['POST', 'GET'])
@logged_in
def submission_status_filter():
    cursor = db.connection.cursor(MySQLdb.cursors.DictCursor) #connect to database

    if request.method == 'POST':
        intern_div = request.form['intern_div']
        print(intern_div)

    """Retrieved intern details"""
    cursor.execute('SELECT * FROM interns WHERE intern_div=%s',[intern_div])
    division = cursor.fetchall()

    # Getting Current Date
    CurrentDate = datetime.datetime.now()

    return render_template('submission_status.html', division=division, CurrentDate=CurrentDate)


@app.route("/staff/submission_status/search",methods=['POST', 'GET'])
@logged_in
def submission_status_search():
    cursor = db.connection.cursor(MySQLdb.cursors.DictCursor) #connect to database

    if request.method == 'POST':
        search_word = request.form['search']
        print(search_word)

        query = "SELECT * from interns WHERE fullname LIKE '%{}%' OR email LIKE '%{}%' OR cost_centre LIKE '%{}%' ORDER BY id DESC LIMIT 20".format(search_word,search_word,search_word)
        cursor.execute(query)
        search = cursor.fetchall()
    
    # Getting Current Date
    CurrentDate = datetime.datetime.now()

    return render_template('submission_status.html', search=search, CurrentDate=CurrentDate)


@app.route('/staff/email_editor' , methods=['POST', 'GET'])
@logged_in
def email_editor():
    cursor = db.connection.cursor(MySQLdb.cursors.DictCursor) #connect to database

    """Retrieved intern details"""
    cursor.execute('SELECT * FROM interns')
    intern = cursor.fetchall()

    if request.method == 'POST':
        recipients = request.form.get('recipients')
        subject = request.form.get('subject')
        files = request.files.getlist("files")
        content = request.form.get('content')

        if not files == " ":
            for file in files:

                types = "hr_onboarding"
                filename = f'{file.filename}'
                file.save(os.path.join(app.config['HR_UPLOADS'], filename))
                cursor.execute("INSERT INTO document_uploads(type, filename, intern_id) VALUES(%s,%s,%s)", [types,filename,intern['id']])
                db.connection.commit()
                print(f'{ types } is uploaded!')
                flash(f'File has uploaded!', 'success')

        data = {}
        # content = render_template('emailcontents/hr_onboarding.html')

        data["subject"] = subject
        data["recipient"] = recipients
        data["body"] = content
        send_email(data)
        flash('Email sent successfully to interns!', 'success')
        return redirect(url_for('email_editor'))

    return render_template('email_editor.html')

@app.route('/staff/email_editor/upload' , methods=['POST', 'GET'])
def email_editor_upload():
    cursor = db.connection.cursor(MySQLdb.cursors.DictCursor) #connect to database

    """Retrieved intern details"""
    cursor.execute('SELECT * FROM interns')
    intern = cursor.fetchall()

    if request.method == 'POST':
        files = request.files.getlist("files")

        if not files == " ":
            for file in files:

                types = "hr_onboarding"
                filename = f'{file.filename}'
                file.save(os.path.join(app.config['FILE_UPLOADS'], filename))
                cursor.execute("INSERT INTO document_uploads(type, filename, intern_id) VALUES(%s,%s,%s)", [types,filename,intern['id']])
                db.connection.commit()
                print(f'{ types } is uploaded!')
                flash(f'File has uploaded!', 'success')
        return redirect(url_for('email_editor'))


@app.route('/staff/manage_files')
@logged_in
def manage_files():
    cursor = db.connection.cursor(MySQLdb.cursors.DictCursor) #connect to database

    """Retrieved intern details"""
    cursor.execute('SELECT * FROM interns')
    data = cursor.fetchall()
    return render_template('manage_files.html', interns=data)

@app.route("/staff/manage_files/search",methods=['POST', 'GET'])
@logged_in
def manage_files_search():
    cursor = db.connection.cursor(MySQLdb.cursors.DictCursor) #connect to database

    if request.method == 'POST':
        search_word = request.form['search']
        print(search_word)

        query = "SELECT * from interns WHERE fullname LIKE '%{}%' OR email LIKE '%{}%' OR cost_centre LIKE '%{}%' ORDER BY id DESC LIMIT 20".format(search_word,search_word,search_word)
        cursor.execute(query)
        search = cursor.fetchall()
    return render_template('manage_files.html', search=search)


@app.route('/staff/manage_intern/<id>')
@logged_in
def manage_intern(id):
    cursor = db.connection.cursor(MySQLdb.cursors.DictCursor) #connect to database

    """Retrieved intern details"""
    cursor.execute('SELECT * FROM interns WHERE id=%s', [id])
    data = cursor.fetchall()
    return render_template('manage_intern.html', interns=data[0])

@app.route('/staff/manage_intern/onboarding/<id>')
@logged_in
def manage_intern_onboarding(id):
    cursor = db.connection.cursor(MySQLdb.cursors.DictCursor) #connect to database

    """Retrieved intern details"""
    cursor.execute('SELECT * FROM interns WHERE id=%s', [id])
    data = cursor.fetchall()

    cursor.execute('SELECT * FROM document_uploads '
                   'WHERE type = "secrecy_agreement" AND intern_id = %s', [id])
    secrecy = cursor.fetchall()

    cursor.execute('SELECT * FROM document_uploads '
                   'WHERE type = "indemnity_letter" AND intern_id = %s', [id])
    indemnity = cursor.fetchall()

    cursor.execute('SELECT * FROM document_uploads '
                   'WHERE type = "insurance_letter" AND intern_id = %s', [id])
    insurance = cursor.fetchall()

    cursor.execute('SELECT * FROM document_uploads '
                   'WHERE type = "offer_letter" AND intern_id = %s', [id])
    offer = cursor.fetchall()

    cursor.execute('SELECT * FROM document_uploads '
                   'WHERE type = "bank_statement" AND intern_id = %s', [id])
    bank = cursor.fetchall()

    cursor.execute('SELECT * FROM document_uploads '
                   'WHERE type = "copy_nric" AND intern_id = %s', [id])
    ic = cursor.fetchall()

    return render_template('manage_intern_onboarding.html', interns=data[0], secrecy_agreement=secrecy, indemnity_letter=indemnity, insurance_letter=insurance, offer_letter=offer, bank_statement=bank, copy_nric=ic)

@app.route('/staff/manage_intern/attendance/<id>')
@logged_in
def manage_intern_attendance(id):
    cursor = db.connection.cursor(MySQLdb.cursors.DictCursor) #connect to database

    """Retrieved intern details"""
    cursor.execute('SELECT * FROM interns WHERE id=%s', [id])
    data = cursor.fetchall()

    cursor.execute('SELECT * FROM attendance_sheet '
                   'WHERE status = "complete" AND intern_id = %s', [id])
    attendance = cursor.fetchall()

    return render_template('manage_intern_attendance.html', interns=data[0], attendance=attendance)

@app.route('/staff/manage_intern/leave/<id>')
@logged_in
def manage_intern_leave(id):
    cursor = db.connection.cursor(MySQLdb.cursors.DictCursor) #connect to database

    """Retrieved intern details"""
    cursor.execute('SELECT * FROM interns WHERE id=%s', [id])
    data = cursor.fetchall()

    cursor.execute('SELECT * FROM document_uploads '
                   'WHERE type = "leave" AND intern_id = %s', [id])
    leave = cursor.fetchall()

    return render_template('manage_intern_leave.html', interns=data[0], leave=leave)

@app.route('/staff/manage_intern/feedback/<id>')
@logged_in
def manage_intern_feedback(id):
    cursor = db.connection.cursor(MySQLdb.cursors.DictCursor) #connect to database

    """Retrieved intern details"""
    cursor.execute('SELECT * FROM interns WHERE id=%s', [id])
    data = cursor.fetchall()

    cursor.execute('SELECT * FROM document_uploads '
                   'WHERE type = "feedback" AND intern_id = %s', [id])
    feedback = cursor.fetchall()

    return render_template('manage_intern_feedback.html', interns=data[0], feedback=feedback)

@app.route('/test')
def test():
    return render_template('test.html')

# Pass the email details to be sent with attachments
def send_email(data):
    #initialization
    msg = MIMEMultipart('alternative')
    keys = []
    values = []
    files = []
    items = data.items()
    for item in items:
        keys.append(item[0]), values.append(item[1])

    cursor = db.connection.cursor(MySQLdb.cursors.DictCursor)

    #Check type of email sent and retrieved the type of file as attachments
    if len(values) > 3:
        if values[3] == 'attendance':
            cursor.execute('SELECT * FROM attendance_sheet WHERE id=%s', [values[4]])
            attendance = cursor.fetchone()

            cursor.execute('SELECT * FROM interns WHERE id=%s', [session['id']])
            intern = cursor.fetchone()

            folder = os.path.join(app.config['FILE_UPLOADS'], values[3])
            for filename in os.listdir(folder):
                if attendance['attendance_month'] in filename and intern['fullname'] in filename:
                    files.append(filename)

            for f in files:
                file_path = os.path.join(folder, f)
                attachment = MIMEApplication(open(file_path, "rb").read(), _subtype="txt")
                attachment.add_header('Content-Disposition', 'attachment', filename=f)
                msg.attach(attachment)
        elif values[3] == 'leave':
            cursor.execute('SELECT * FROM interns WHERE id=%s', [values[4]])
            intern = cursor.fetchone()

            cursor.execute('SELECT * FROM request_leave WHERE intern_id=%s ORDER BY id DESC LIMIT 1', [values[4]])
            leave = cursor.fetchone()

            folder = os.path.join(app.config['FILE_UPLOADS'], values[3])
            for filename in os.listdir(folder):
                if str(leave['id']) in filename and intern['fullname'] in filename:
                    files.append(filename)

            for f in files:
                file_path = os.path.join(folder, f)
                attachment = MIMEApplication(open(file_path, "rb").read(), _subtype="txt")
                attachment.add_header('Content-Disposition', 'attachment', filename=f)
                msg.attach(attachment)

        else:
            # Retrieved intern data
            cursor.execute('SELECT * FROM interns WHERE id=%s', [values[4]])
            intern = cursor.fetchone()

            folder = os.path.join(app.config['FILE_UPLOADS'], values[3])
            for filename in os.listdir(folder):
                if intern['fullname'] in filename:
                    files.append(filename)

            for f in files:
                file_path = os.path.join(folder, f)
                attachment = MIMEApplication(open(file_path, "rb").read(), _subtype="txt")
                attachment.add_header('Content-Disposition', 'attachment', filename=f)
                msg.attach(attachment)


        msg['Subject'] = values[0]
        msg['From'] = EMAIL_ADDRESS
        msg['To'] = values[1]

        body = MIMEText(values[2], 'html', 'utf-8')
        msg.attach(body)
    else:
        msg['Subject'] = values[0]
        msg['From'] = EMAIL_ADDRESS
        msg['To'] = values[1]

        body = MIMEText(values[2], 'html', 'utf-8')
        msg.attach(body)

    with smtplib.SMTP_SSL('smtp.gmail.com', 465) as smtp:
        smtp.login(EMAIL_ADDRESS, EMAIL_PASSWORD)

        smtp.send_message(msg)
        print('Done!')
        del msg


if __name__ == '__main__':
    app.run(debug=True)

